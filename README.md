# data-itrf

Données relatives aux concours et recrutements des personnels ITRF de l'ESR

En cours...


## Années traitées

Une année traitée implique qu'on été enregistrées les ouvertures de postes dans : 
- les catégories A, B et C
- via les concours dits externes, internes, 3e concours et réservés

...

## Années manquantes ou partielles

...

## sources 

* en partie depuis Legifrance
* en partie depuis les tableurs publiés par le ministère de l'ESR et disponibles dans le dossier ./documents de ce dépôt

...
